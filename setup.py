from setuptools import setup


# def readme():
#     with open('README.rst') as f:
#         return f.read()


setup(name='google_reviews',
      version='0.3',
      description="Extracts all metrics on a given company's glassdoor page using the webpage itself and the unofficial API.",
      #   long_description=readme(),
      url='https://bitbucket.org/flamingodf/google_reviews/',
      author='Chris Daly',
      author_email='chris.daly@flamingogroup.com',
      license='MIT',
      packages=['google_reviews'],
      install_requires=['tqdm', 'bs4', 'requests', 'pandas', ],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose']
      )
