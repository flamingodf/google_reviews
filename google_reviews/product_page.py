from bs4 import BeautifulSoup as BS


class ProductPage:
    def __init__(self, soup):
        self.soup = soup
        # with open("./dump.html", "w") as f:
        #     f.write(str(self.soup))

    @property
    def price(self):
        spans = self.soup.find_all("span")
        if spans:
            price = spans[0].get_text()
            return price

    @property
    def description(self):
        details = self.soup.find_all("div", string="PRODUCT DETAILS")
        if details:
            details = details[0]
            description = details.parent.find_all("div")[1].get_text()
            return description

    @property
    def image(self):
        image = self.soup.find("img")
        if image is not None:
            return image["src"]

    def parse(self):
        data = {
            "price": self.price,
            "description": self.description,
            "image": self.image
        }
        return data
