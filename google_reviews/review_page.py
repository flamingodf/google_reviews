from bs4 import BeautifulSoup as BS
from .review import Review


class ReviewPage:
    def __init__(self, soup):
        self.soup = soup

    @property
    def product_name(self):
        return self.soup.find("a", class_="translate-content").get_text()

    @property
    def product_id(self):
        return self.soup.find("a", {"class": "sh-t__title"})["href"].split("/")[-1]

    @property
    def ratings(self):
        review_metrics = {}
        rating_anchors = self.soup.find("div", {"class": "sh-rov__hist-container"}).find_all("a")
        for rating_anchor in rating_anchors:
            text = rating_anchor.find_all("div")[1]["aria-label"]
            review_count, num_stars, _, _ = text.split()
            review_metrics[num_stars] = review_count.replace(",", "").replace("k+", "000").replace("m+", "000000")
        return review_metrics

    @property
    def reviews(self):
        review_container = self.soup.find("div", {"id": "sh-rol__reviews-cont"})
        if review_container is not None:
            reviews = list(review_container.children)
            return reviews
        else:
            print("NO CONTAINERS")
            print(self.soup)

    def parse(self):
        data = {
            "product_name": self.product_name,
            "product_id": self.product_id,
            "ratings": self.ratings
        }
        return data
