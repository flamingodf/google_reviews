import time
from typing import NewType

import requests
import tqdm
from bs4 import BeautifulSoup
from google_reviews import Review, ReviewPage

from .review import Review
from .review_page import ReviewPage
from .product_page import ProductPage

Soup = NewType("Soup", BeautifulSoup)


def get_soup(url: str, headers: dict = None, params: dict = None, dump: bool = False, sleep: int = 1) -> Soup:
    time.sleep(sleep)
    response = requests.get(url, headers=headers, params=params)
    # print(response.url)
    if response.status_code != 200:
        return None

    text = response.text
    if dump:
        with open("test.html", "w") as f:
            f.write(text)

    soup = BeautifulSoup(text, features="lxml")

    return soup


def create_params(product_id: str, num_per_page: str, start: str, rating: str = None, output_format: str = "html") -> dict:
    params = {'fmt': output_format}
    filters = f"cid:{product_id},cs:0,rnum:{num_per_page},rstart:{start},sgro:or"
    if rating:
        filters += ",rate:{rating}".format(rating=rating)
    params.update({'prds': filters})

    return params


def get_product_meta(product_id: str, sleep: int = 1) -> dict:
    time.sleep(sleep)
    response = requests.get(f"https://www.google.com/shopping/product/{product_id}/")
    soup = BeautifulSoup(response.text, features="lxml")
    product_page = ProductPage(soup)
    data = product_page.parse()
    return data


def scrape(product_id: str) -> dict:
    headers = {
        'authority': 'www.google.com',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
        'accept-language': 'en-US,en;q=0.9'}

    num_per_page = 100
    start = 0
    output_format = "html"  # More structured than Google's malformed json.
    url = f'https://www.google.com/shopping/product/{product_id}/reviews'
    data = {"reviews": []}

    product_data = get_product_meta(product_id)
    data.update(product_data)

    # Parsing the review page meta data.
    rating = 0
    params = create_params(product_id, num_per_page, start, rating, output_format="html")
    soup = get_soup(url, headers=headers, params=params)

    review_page = ReviewPage(soup)
    data.update(review_page.parse())

    for rating in range(1, 6):
        start = 0
        id_old = ""
        attempts = 0
        attempts_max = 1
        num_reviews = 0
        num_reported_reviews_for_rating = data["ratings"].get(str(rating), 0)
        num_reported_reviews_for_rating = int(num_reported_reviews_for_rating)
        id_old = None

        params = create_params(product_id, num_per_page, start, rating, output_format="html")
        # print("\n\nRating: {}\nMax reviews: {}".format("⭐"*int(rating), num_reported_reviews_for_rating))
        while True:
            # Break when we have collected all the reported views.
            if num_reviews == num_reported_reviews_for_rating:
                # print("\t\t\t\t\t====\n\t\t\t\t\t {}".format(num_reviews))
                # print("REACHED REPORTED NUMBER OF REVIEWS")
                break

            soup = get_soup(url, headers=headers, params=params)
            review_page = ReviewPage(soup)
            reviews_divs = review_page.reviews

            # Went beyond the end of a rating with less than 1100 reviews.
            if not reviews_divs:
                # print("\t\t\t\t\t=====\n\t\t\t\t\t   {}".format(num_reviews))
                continue

            id_new = Review(reviews_divs[0]).parse()["id"]
            # print("Page: {} \t Attempt: {} \t Reviews: {} \t {}".format(start, attempts, len(reviews_divs), id_new))

            # If we have a new top page review id, parse and add the rest.
            if id_old != id_new:
                # print("NEW REVIEWS", id_old, id_new)
                for review_soup in reviews_divs:
                    review = Review(review_soup).parse()
                    data["reviews"].append(review)

                num_reviews += len(reviews_divs)
                id_old = id_new
                start += num_per_page

            # If we have a repeat, google is being reluctant to give new reviews, try a few times.
            else:
                if (attempts < attempts_max):
                    attempts += 1
                    continue
                else:
                    # print("\t\t\t\t\t====\n\t\t\t\t\t {}".format(num_reviews))
                    # print("MAX ATTEMPTS")
                    break
    return data
