import unicodedata
from bs4 import BeautifulSoup
import datetime


class Review:
    def __init__(self, soup):
        self.soup = soup

    @property
    def id_(self):
        return self.soup.find_all("div", {"id": lambda x: x is not None})[0]["id"].split("-full")[0].split("-short")[0]

    @property
    def person(self):
        person = self.soup.find_all("div")[-1].next_element
        # if not isinstance()
        person = person.string
        # person = unicodedata.normalize("NFKD", person)
        return person

    @property
    def website(self):
        return self.soup.find_all("div")[-1].find_all("span")[-1].get_text(strip=True)

    @property
    def title(self):
        test = self.soup.find("div")
        if test.div is None:
            title = test.get_text()
            return title

    @property
    def text(self):
        text = self.soup.find("div", {"id": lambda x: (x is not None) and (x.endswith("-full"))})

        if text is not None:
            span = text.find("span")  # More/Less span click
            if span is not None:
                span.extract()
            text = unicodedata.normalize("NFKD", text.get_text(strip=True))
            return text
        else:
            print(self.soup)

    @property
    def rating(self):
        return self.soup.find("div", {"aria-label": lambda x: x is not None})["aria-label"].split()[0]

    @property
    def timestamp(self):
        date_text = self.soup.find("div", {"aria-label": lambda x: x is not None}).nextSibling.get_text()
        return datetime.datetime.strptime(date_text, "%d %B %Y").timestamp()

    @property
    def needs_translating(self):
        needs_translating = self.soup.find("div", {"id": lambda x: (x is not None) and (x.endswith("-full-transLink"))})
        if needs_translating is not None:
            return True

        else:
            return False

    def parse(self):
        data = {
            "id": self.id_,
            "person": self.person,
            "website": self.website,
            "title": self.title,
            "rating": self.rating,
            "text": self.text,
            "timestamp": self.timestamp,
            "needs_translating": self.needs_translating
        }

        return data
